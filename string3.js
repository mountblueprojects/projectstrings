// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.
function problem3(string){
    if(typeof string!=='string'){
        return [];
    }
    if(typeof(parseInt(string[3]))==='number'){
        if(string[4]!=='/'){
            if(parseInt(string[3])!=1 || parseInt(string[4])>2){
                return [];
            }
            if(string[4]==='1'){
                return 'November';
            } else{
                return 'December';
            }
        } else{
            switch(string[3]){
                case '1':
                    return 'January';
                    break;
                case '2':
                    return 'February';
                    break;
                case '3':
                    return 'March';
                    break;
                case '4':
                    return 'April';
                    break;
                case '5':
                    return 'May';
                    break;
                case '6':
                    return 'June';
                    break;
                case '7':
                    return 'July';
                    break;
                case '8':
                    return 'August';
                    break;
                case '9':
                    return 'September';
                    break;
            }
        }
        
    } else{
        return [];
    }
}
module.exports=problem3;