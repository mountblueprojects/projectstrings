// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 
function problem1(string){
    if(typeof string!=='string' || string.length===0){
        return [];
    }
    let numbers=string.split('$');
    let minus=0;
    while(numbers[0]==='-'){
        minus++;
        if(minus>1){
            return [];
        }
        numbers.shift();
    }
    while(numbers[0].length===0){
        numbers.shift();
    }
    if(numbers.length>1){
        return [];
    }
    let number=parseFloat(numbers[0]);
    if(number.toString().length!==numbers[0].length){
        return [];
    }
    else{
        if(minus){
            return -1*number;
        }
        else{
            return number;
        }
    }   
}
module.exports=problem1;