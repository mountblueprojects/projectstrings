// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.
function problem2(string){
    if(typeof string!=='string'){
        return [];
    }
    let ip=string;
    ip=ip.split('.');
    if(ip.length!==4){
        return [];
    }
    let ipArray=[];
    for(let i=0;i<ip.length;i++){
        if(ip[i].length>3||parseInt(ip[i][0])>2){
            return [];
        }
        if(parseInt(ip[i][0])===2 && parseInt(ip[i][1])>5){
            return [];
        }
        if(parseInt(ip[i][0])===2 && parseInt(ip[i][1])===5 && parseInt(ip[i][2])>5){
            return [];
        }        
        ipArray.push(parseInt(ip[i]));      
    }
    let count=0;
    for(let i=0;i<ipArray.length;i++){        
        if(ip[i].length!==ipArray[i].toString().length){
            return [];
        }
    }
    return ipArray;    
}
module.exports=problem2;