// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.
function problem5(array){
    if(!Array.isArray(array)){
        return 0;
    }
    let result='';
    for(let i=0;i<array.length;i++){
        result+=array[i]+' ';    
    }
    return result;
}
module.exports = problem5;