// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
function problem4(object){
    if(typeof object!=='object'){
        return [];
    }
    if(typeof object==='object'){
        if(Array.isArray(object)){
            return [];
        }
    }
    let name=object.first_name+' '+object.middle_name+' '+object.last_name;
    name=name.toLowerCase();
    name=name.split(' ');
    for(let i=0;i<name.length;i++){        
        name[i]=name[i].charAt(0).toUpperCase()+name[i].slice(1);
    }
    let fullName='';
    for(let i=0;i<name.length;i++){
        if(name[i]==='Undefined'){
            continue;
        }
        fullName+=name[i]+' ';
    }
    if(fullName===''){
        return [];
    }
    return fullName;   
}
module.exports=problem4;